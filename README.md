# Belajar Catur

Kumpulan sumber untuk belajar catur bagi kroco-kroco kaya saya.

## Daftar Isi

- [Level Pemain](#level-pemain)
- [Buku](#buku)
- [Artikel](#artikel)
- [YouTube](#youtube)
  * [Video](#video)
    + [Opening](#opening)
    + [Middlegame](#middlegame)
    + [Endgame](#endgame)
    + [Untuk Pemula](#untuk-pemula)
    + [Lichess Study](#lichess-study)
    + [Umum](#umum)
  * [Playlist](#playlist)
    + [Opening](#opening-1)
    + [Endgame](#endgame-1)
    + [Umum](#umum-1)
  * [Channel](#channel)
- [Aplikasi](#aplikasi)
  * [Platform](#platform)
  * [Opening](#opening-2)
    + [Lichess Study](#lichess-study-1)
  * [Puzzle](#puzzle)
  * [Analisis](#analisis)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Level Pemain

Level ini akan dipakai untuk membahas sumber-sumber belajar catur.

- `Beginner`: belum tahu apa-apa tentang catur.
- `Novice` (ELO 400-600): tahu aturan catur, tapi belum bisa menang.
- `Amateur` (ELO 600-800): sudah bisa menang kalau lawannya belum *proper* main caturnya.
- `Pre-intermediate` (ELO 800-1000): sudah tau beberapa konsep *opening*, tapi masih perlu belajar untuk *middlegame*, *endgame*, variasi *opening*.
- `Intermediate` (ELO 1000-1500): sudah bisa *opening*, variasi *opening*, *middlegame*, dan *endgame*, tapi kurang *counter-play* dan evaluasi *look ahead* (simulasi permainan di otak).
- `Advanced` (ELO > 1500): sudah profesional / sudah jago.

## Buku

Daftar lengkap bisa dilihat di [wishlist ini](https://www.tokopedia.com/wishlist/collection/2398666) (khusus Dejan).

- [Chess and the Art of War: Ancient Wisdom to Make You a Better Player](https://www.goodreads.com/book/show/25319055-chess-and-the-art-of-war)
    - Bagus untuk `Beginner` dan `Novice`.
    - Untuk `Amateur` ke atas keanya perlu cari sumber lain.
- [Buku catur anak seri 4 buku](https://www.tokopedia.com/doktercatur/buku-catur-anak-seri-4-buku), yang terdiri dari 4 buku:
    - How to beat your Dad
    - Chess Strategy and Tactics for kids
    - Chess Openings for kids
    - Chess Endgames for kids
- Power Chess for Kids
    - [Volume 1](https://www.tokopedia.com/doktercatur/buku-catur-anak-power-chess-for-kids)
    - [Volume 2](https://www.tokopedia.com/adreenachess/buku-catur-anak-power-chess-for-kids-volume-2-by-charles-hertan-softcover)
- [Winning Chess Strategies](https://www.tokopedia.com/rcbook/buku-catur-winning-chess-strategies-by-yasser-seirawan)
- [100 Endgame Patterns You Must Know](https://www.tokopedia.com/rcbook/buku-catur-100-endgame-patterns-you-must-know-by-jesus-de-la-villa)
- [Silman's Complete Endgame Course](https://www.tokopedia.com/amazonbookgrosir/buku-silman-s-complete-endgame-course-by-jeremy-silman)
- [Dvoretsky's Endgame Manual](https://www.tokopedia.com/adreenachess/buku-catur-dvoretsky-s-endgame-manual-5th-new-edition-chess-dvoretsky-softcover)
    - Untuk level `Intermediate` ke atas.
- [The Amateur's Mind: Turning Chess Misconceptions Into Chess Mastery](https://www.tokopedia.com/bookxuply/the-amateur-s-mind-turning-chess-misconceptions-into-chess-mastery)
    - Katanya bagus untuk `Novice` dan `Amateur`.
    - Buku home-made lokal bisa dicari [di sini](https://www.tokopedia.com/85zahra/the-amateur-s-mind-turning-chess-misconceptions-into-chess).
- [The Woodpecker Method](https://www.tokopedia.com/adreenachess/buku-catur-the-woodpecker-method-by-axel-smith-hans-tikkanen-softcover)

## Artikel

- [Kumpulan artikel oleh Dan Heisman](https://www.danheisman.com/articles-by-subject.html)
    - Katanya bagus untuk `Novice` dan `Amateur`, terutama mengasah pola pikir permainan.
- [Rekomendasi buku catur oleh Dan Heisman](https://www.danheisman.com/recommended-books.html)
- Checks, Captures, and Attacks
    - [Check, Capture , and Attack](https://www.chess.com/forum/view/general/check-capture-and-attack)
        - Diskusi di forum Chess.com tentang prinsip ini. Ada beberapa pemikiran yang menarik.
    - [Checks, Captures, Queen Attacks!](https://www.chesskid.com/learn/articles/checks-captures-queen-attacks) oleh Daniel Rensch, International Master dan "Chief Chess Officer" dari [Chess.com](chess.com).
- [How to Find the Right Chess Coach](https://lichess.org/@/Avetik_ChessMood/blog/how-to-find-the-right-chess-coach--ultimate-guide/xpG0XDFB)
- [How to Handle Painful Blunders](https://lichess.org/@/Avetik_ChessMood/blog/how-to-handle-painful-blunders/cUxb5AKv)
- [Top 7 Chess Calculation Mistakes to Avoid](https://thechessworld.com/articles/general-information/top-7-chess-calculation-mistakes-to-avoid/)
- [Beginner Chess Strategy: 5 Must-Know Tips](https://nextlevelchess.blog/beginner-chess-strategy/) oleh GM Noel Studer
- [Improve Your Chess Tactics](https://nextlevelchess.blog/improve-tactics/) oleh GM Noel Studer
- [How To Analyze Your Own Chess Game: Part 1](https://nextlevelchess.blog/game-analysis1/) oleh GM Noel Studer

## YouTube

### Video

#### Opening

- [How To Learn & Study Chess Openings](https://www.youtube.com/watch?v=6IegDENuxU4) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [Brutally PUNISH Scholar's Mate & Early Queen Attacks](https://youtu.be/2a3wDY69NJI) oleh [Remote Chess Academy](https://www.youtube.com/@GMIgorSmirnov)
- [5 Aggressive Ideas to PUNISH Bg4 Pin on Your Knight [Brutal TACTICS]](https://www.youtube.com/watch?v=bTudfm9GkdY) oleh [Remote Chess Academy](https://www.youtube.com/@GMIgorSmirnov)
- [10 Chess Traps to Win FAST!!](https://www.youtube.com/watch?v=fotEfEXL60o) oleh [GothamChess](https://www.youtube.com/@GothamChess)

#### Middlegame

- [How To Analyze Your Chess Games](https://youtu.be/ylpAHvPlafc) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [How To Calculate In Chess](https://www.youtube.com/watch?v=9Ga9dP3bvN8) oleh [GothamChess](https://www.youtube.com/@GothamChess)
    - Bagaimana cara "menghitung" (lebih tepatnya, cara menentukan) "next move" / jalan berikutnya bagi `Novice` dan `Amateur`. Konsep "Checks, Captures, and Attacks" dibahas juga di sini.
- [The BEST Way To Calculate in Chess](https://www.youtube.com/watch?v=AN4gqEqIXm0) oleh [Anna Cramling](https://www.youtube.com/@AnnaCramling)
- [Top 18 Attacking Principles/Concepts In Chess - How To Attack Correctly - How To Sacrifice Pieces!](https://www.youtube.com/watch?v=o7ALEPcUaSU) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)
- [Top 18 Attacking Principles/Concepts In Chess - How To Attack Correctly - How To Sacrifice Pieces!](https://www.youtube.com/watch?v=o7ALEPcUaSU)

#### Endgame

- [6 Checkmate Patterns YOU MUST KNOW](https://www.youtube.com/watch?v=iBZLU1FXhcI) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [5 Chess Endgames You MUST Know](https://www.youtube.com/watch?v=Qp_rB1fN3Gs) oleh [Anna Cramling](https://www.youtube.com/@AnnaCramling)
- [Top 25 Chess Endgame Principles - Endgame Concepts, Ideas | Basic Chess Endgame Principles and Plans](https://www.youtube.com/watch?v=XZeTKbB_LWg) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)

#### Untuk Pemula

- [Under 800 Rated? Watch This.](https://www.youtube.com/watch?v=CepTyY9NHGM) oleh [Anna Cramling](https://www.youtube.com/@AnnaCramling)
- [Under 1200 Rated? Watch This.](https://www.youtube.com/watch?v=qmJD2S3oy4M) oleh [Anna Cramling](https://www.youtube.com/@AnnaCramling)
- [Want to be 1000 in chess?](https://youtu.be/mjZKRoNuAvY) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [7 MOST COMMON Chess Mistakes](https://www.youtube.com/watch?v=SXrKRA_KZ5k) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [EVERY Chess Beginner Should Watch This Lesson](https://www.youtube.com/watch?v=00Nfr_FfHwA) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [25 Chess Principles For Beginners (with examples)](https://youtu.be/bew7T426qeA) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)
- [Stuck At Chess? Click here.](https://youtu.be/bK6rAfukA44) oleh [GothamChess](https://www.youtube.com/@GothamChess)

#### Lichess Study

- [Building an Opening Repertoire using lichess.org](https://www.youtube.com/watch?v=xGdqaXZWZC8) oleh [Eric Rosen](https://www.youtube.com/@eric-rosen)
- [How to use Lichess Studies! A tool EVERY chess player should take advantage of!](https://www.youtube.com/watch?v=LQdg3YQBoUs) oleh [ZR CHESS](https://www.youtube.com/@zrchess7052)

#### Umum

- [Top 4 Most Overrated Chess Books (and what you should read instead)](https://www.youtube.com/watch?v=EfFNcUZ15no) oleh [IM Kostya Kavutskiy](https://www.youtube.com/@IMKostyaKavutskiy)
    - Video tentang review buku bagi pemula (`Novice` dan `Amateur`) dan kenapa buku yang populer belum tentu cocok bagi pemula.
    - Tapi saya sendiri belum ada buku-buku ini, nanti mungkin bisa dimasukin ke section "Buku" di atas. 
- [35 Vital Chess Principles | Opening, Middlegame, and Endgame Principles - Chess Strategy and Ideas](https://www.youtube.com/watch?v=nXyJdetptXg) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)
- [Chess Visualizing: How To Remember Squares](https://www.youtube.com/watch?v=-i9wcusTwSo) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [Stopping Early Queen Attacks In Chess](https://www.youtube.com/watch?v=cY9zitJFglc) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [Want To Change Your Life?](https://www.youtube.com/watch?v=UsgaqyO6OZg) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [How to ACTUALLY Improve your Chess Tactics](https://www.youtube.com/watch?v=1qicV-jaGFE) oleh [FM Jack Puccini - Chess](https://www.youtube.com/@fmjackpuccini-chess6740)
- [How To Improve Your Chess Psychology](https://youtu.be/mIZSNGPdxo8) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [4 Times Castling In Chess Is Bad](https://youtu.be/VSEJiuwQLjI) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)
- [How to Beat GothamChess](https://youtu.be/uSuRmg9HM4E) oleh [ChessGoals](https://www.youtube.com/@ChessGoals)
    - Video tentang cara analisis game orang lain.
    - Hanya bisa di chess.com.
- [8 Chess Concepts Every Chess Player Should Know - Chess Principles, Ideas, Strategies and Tips](https://www.youtube.com/watch?v=aRy8A3-1A9I) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)
- [21 Positional Chess Concepts   -   Chess Strategies You NEED TO KNOW - Chess Principles and Ideas](https://www.youtube.com/watch?v=BYtXMJjqfvw) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)
- [10 Ways To Save The Game In Chess](https://www.youtube.com/watch?v=BnOw3gMgBOs) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)

### Playlist

#### Opening

- [Chess Openings](https://www.youtube.com/playlist?list=PLBRObSmbZluTpMdP-rUL3bQ5GA8v4dMbT) oleh [GothamChess](https://www.youtube.com/@GothamChess)

#### Middlegame

- [Chess Tips](https://www.youtube.com/playlist?list=PLBRObSmbZluSEeBkH17c72MaM4nFjqoi8) oleh [GothamChess](https://www.youtube.com/@GothamChess)

#### Endgame

- [Endgame Course](https://www.youtube.com/playlist?list=PLp7SLTJhX1u4rgJNaP2UOT0xyG3Y9QaJ2) oleh [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)
- [End Game](https://www.youtube.com/playlist?list=PLT1F2nOxLHOfQI_hFiDnnWj4lb5KsviJ_) oleh [Daniel Naroditsky](https://www.youtube.com/@DanielNaroditskyGM)
- [ENDGAMES](https://www.youtube.com/playlist?list=PLBRObSmbZluTCoqlOg-3wEdlyRQLAJ2gs) oleh [GothamChess](https://www.youtube.com/@GothamChess) 

#### Umum

- [Chess Steps](https://www.youtube.com/playlist?list=PLBRObSmbZluSOrSTYyeU8v2ZWL-fqNuFB) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [Win at Chess](https://www.youtube.com/playlist?list=PLBRObSmbZluSo6h0AySyeZRdlQzEhr2XL) oleh [GothamChess](https://www.youtube.com/@GothamChess)
- [LUCAS CHESS](https://www.youtube.com/playlist?list=PLaY9haerg5XwVBlPiV-xAsYTqxGV16T6B) oleh [CJ CHESS HORIZON](https://www.youtube.com/@cjchesshorizon9627)

### Channel

Channel yang di sini channel yang memang untuk `Amateur` ke bawah, walaupun ada konten untuk `Pre-intermediate` ke atas. 

- [Remote Chess Academy](https://www.youtube.com/@GMIgorSmirnov)
- [Chess Vibes](https://www.youtube.com/@ChessVibesOfficial)
- [GothamChess](https://www.youtube.com/@GothamChess)
- [Anna Cramling](https://www.youtube.com/@AnnaCramling)

## Aplikasi

### Platform

- [Chess.com](https://chess.com)
    - Aplikasi Android: [Chess - Play and Learn](https://play.google.com/store/apps/details?id=com.chess)
    - Untuk akses beberapa fitur, harus bayar.
- [Lichess](https://lichess.org)
    - Aplikasi Android: [lichess • Free Online Chess](https://play.google.com/store/apps/details?id=org.lichess.mobileapp)
    - Gratis dan [open source](https://github.com/lichess-org).

Perbandingan platform online catur:
- [Chess.com vs Chess24 vs Lichess: The Ultimate Review](https://chesscience.com/chess-com-vs-chess24-vs-lichess-the-ultimate-review/)
- [The 10 best places to play chess online](https://www.chessstrategyonline.com/play-chess-online)

### Opening

- [Chess Openings Pro](https://play.google.com/store/apps/details?id=net.lrstudios.chess_openings)
- [Chess Openings Trainer Lite](https://play.google.com/store/apps/details?id=com.beadapps.chessrepertoiretrainer.free)

#### Lichess Study

- [Vienna Opening - Gotham Chess](https://lichess.org/study/PcL5mKZ5)

### Puzzle

- [Chess Tactics Pro (Puzzles)](https://play.google.com/store/apps/details?id=net.lrstudios.android.chess_problems)
    - Install di handphone dan/atau tablet Android.
    - Koleksi puzzle yang banyak.
    - Ada puzzle baru setiap hari.
- [Puzzle themes - lichess.org](https://lichess.org/training/themes)
    - Puzzle yang gak hanya *middlegame* dan *endgame*, tapi juga ada untuk *opening* dan berbagai macam serangan.
- [Lichess Puzzle Themes](https://lichess.org/training/themes)
    - Harus daftar Lichess dahulu.
    - Puzzle dibagi ke kategori-kategori kea `Opening`, `Endgame`, `Rook endgame`, dan lain-lain.

### Analisis

- [Lucas Chess](https://lucaschess.pythonanywhere.com)
    - Bisa di install di Windows dan Linux.
    - Tersedia dalam bentuk aplikasi portable di [PortableApps](https://portableapps.com/apps/games/lucas-chess-portable).
    - Dokumentasi lengkap bisa dibaca di [sini](https://lucaschess.pythonanywhere.com/docs).
    - Ada fitur "Tutor", di mana salah 1 chess engine (default: Stockfish) akan memberi tahu "next move" terbaik berdasarkan evaluasi chess engine.
    - Ada fitur "Analysis" di mana kita bisa import game (dalam bentuk format PGN), lalu kita analisis tiap move kita dan lawan. Fitur "Analysis" juga bisa digunakan untuk fitur baru.
- [Lichess Analysis Board](https://lichess.org/analysis)
    - Harus daftar Lichess terlebih dahulu.
    - Analisis game secara online, tidak perlu install apapun.
